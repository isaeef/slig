import requests
import json
import urllib.request
import time
NVD_API_KEY = "a05ff1ef-556e-4205-bb50-c3c862f2109a"

CISA_URI = "https://www.cisa.gov/sites/default/files/feeds/known_exploited_vulnerabilities.json"


def get_nvd_data(cve_id):
  headers = {"Authorization": f"Bearer {NVD_API_KEY}"}
  response = requests.get(
      f"https://services.nvd.nist.gov/rest/json/cve/1.0/{cve_id}",
      headers=headers)
  if response.status_code == 200:
    return json.loads(response.content)
  else:
    raise Exception(
        f"Failed to download NVD data for CVE {cve_id}: {response.status_code}"
    )


req = urllib.request.Request(CISA_URI)
with urllib.request.urlopen(req) as url:
  data = json.loads(url.read().decode())

# Update the code to use the get_nvd_data() function to download the NVD data
for i, v in enumerate(data["vulnerabilities"]):
  try:
    data["vulnerabilities"][i]["nvd"] = get_nvd_data(v["cveID"])
    print(".", end="", flush=True)
    time.sleep(20)

  except Exception as e:
    response_url = "none"
    print("\r\n{} for {}\r\n".format(e, response_url))
    continue
